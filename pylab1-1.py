class Student:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def get_score(self):
        print("{0}分数是：{1}".format(self.name, self.age))


s1 = Student("Clichong", 18)
s1.get_score()

s1.salary = 100000  # 直接添加实例salary与score
s1.score = 41
print(s1.salary)

s2 = Student("Xioaqi", 41)
print(s2.age)